package org.openmrs.module.idgen;

import junit.framework.Assert;

import org.junit.Test;
import org.openmrs.test.Verifies;

public class IdgenUtilTest {
	
	char[] baseCharacters = "0123456789ABCDEF".toCharArray();
		
	@Test
	@Verifies(value = "should convert from long to string in base character set", method = "convertToBase(String,char[])")
	public void Convert_from_long_to_string_in_base_character_set() {
		long numericValue = 43804337214L;
		String hexValue = IdgenUtil.convertToBase(numericValue, baseCharacters, 0);
		System.out.println("Converted from numeric: " + numericValue + " to hex: " + hexValue);
		Assert.assertEquals("A32F1243E", hexValue);
	}
	
	@Test
	@Verifies(value = "should convert from string in base character set to long", method = "convertFromBase(String,char[])")
	public void Convert_from_string_in_base_character_set_to_long() {
		long numericValue = 43804337214L;
		long back = IdgenUtil.convertFromBase("A32F1243E", baseCharacters);
		Assert.assertEquals(numericValue, back);	
	}

}
