package org.openmrs.module.idgen;

import org.junit.Assert;
import org.junit.Test;
import org.openmrs.api.context.Context;
import org.openmrs.module.idgen.api.IdentifierSourceService;
import org.openmrs.test.BaseModuleContextSensitiveTest;

public class IdgenUtilTestInMemory extends BaseModuleContextSensitiveTest {

	@Test
	public void testStuff() throws Exception {
		IdentifierSourceService iss = Context.getService(IdentifierSourceService.class);
		IdentifierSource is = iss.getIdentifierSource(6);
		Assert.assertNotNull(is);
		Assert.assertTrue(is instanceof EgsIdentifierGenerator);
		
		EgsIdentifierGenerator sig = (EgsIdentifierGenerator) is;
		System.out.println(sig.getMinLength());
	}
}
