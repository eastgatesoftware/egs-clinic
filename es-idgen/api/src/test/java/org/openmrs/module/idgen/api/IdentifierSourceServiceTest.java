/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.idgen.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.hibernate.NonUniqueResultException;
import org.junit.Before;
import org.junit.Test;
import org.openmrs.Location;
import org.openmrs.PatientIdentifierType;
import org.openmrs.api.LocationService;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.module.idgen.AutoGenerationOption;
import org.openmrs.module.idgen.EgsIdentifierGenerator;
import org.openmrs.module.idgen.IdentifierSource;
import org.openmrs.module.idgen.api.impl.BaseIdentifierSourceService;
import org.openmrs.module.idgen.api.db.IdentifierSourceDAO;
import org.openmrs.module.idgen.processor.EgsIdentifierGeneratorProcessor;
import org.openmrs.module.idgen.processor.IdentifierSourceProcessor;
import org.openmrs.test.BaseModuleContextSensitiveTest;
import org.openmrs.test.Verifies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Tests {@link ${BasicExampleService}}.
 */
public class  IdentifierSourceServiceTest extends BaseModuleContextSensitiveTest {
	
	private BaseIdentifierSourceService iss;
	
	@Autowired
    IdentifierSourceDAO dao;
	
	@Autowired
    @Qualifier("patientService")
    PatientService patientService;

    @Autowired
    @Qualifier("locationService")
    LocationService locationService;
   
    
    @Before
    public void beforeEachTest() throws Exception {

        executeDataSet("TestData.xml");

        iss = new BaseIdentifierSourceServiceStub();
        iss.setDao(dao);

        EgsIdentifierGeneratorProcessor processor = new EgsIdentifierGeneratorProcessor();
        processor.setIdentifierSourceService(iss);

        Map<Class<? extends IdentifierSource>, IdentifierSourceProcessor> processors = new HashMap<Class<? extends IdentifierSource>, IdentifierSourceProcessor>();
        processors.put(EgsIdentifierGenerator.class, processor);
        iss.setProcessors(processors);
    }
    
	@Test
	@Verifies(value = "should return all identifier sources", method = "getAllIdentifierSources(boolean)")
	public void getAllIdentifierSources_shouldReturnAllIdentifierSources() throws Exception {
		List<IdentifierSource>  sig = iss.getAllIdentifierSources(false);
		Assert.assertTrue(sig.size() == 1);
	}
    
	@Test
	@Verifies(value = "should return a saved sequential identifier generator", method = "getIdentifierSource(Integer)")
	public void getIdentifierSource_shouldReturnASavedEgsIdentifierGenerator() throws Exception {
		EgsIdentifierGenerator sig = (EgsIdentifierGenerator)iss.getIdentifierSource(1);
		Assert.assertEquals(sig.getName(), "Test Sequential Generator");
		Assert.assertNotNull(sig.getBaseCharacterSet());
	}

	/**
	 * @see {@link IdentifierSourceService#purgeIdentifierSource(IdentifierSource)}
	 * 
	 */
	@Test
	@Verifies(value = "should delete an IdentifierSource from the system", method = "purgeIdentifierSource(IdentifierSource)")
	public void purgeIdentifierSource_shouldDeleteAnIdentifierSourceFromTheSystem() throws Exception {
		IdentifierSource s = iss.getIdentifierSource(1);
		iss.purgeIdentifierSource(s);
		Assert.assertNull(iss.getIdentifierSource(1));
	}

	/**
	 * @see {@link IdentifierSourceService#saveIdentifierSource(IdentifierSource)}
	 * 
	 */
	@Test
	@Verifies(value = "should save a sequential identifier generator for later retrieval", method = "saveIdentifierSource(IdentifierSource)")
	public void saveIdentifierSource_shouldSaveAEgsIdentifierGeneratorForLaterRetrieval() throws Exception {
		
		String name = "Sample Id Gen";
		String baseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		
		EgsIdentifierGenerator sig = new EgsIdentifierGenerator();
		sig.setName(name);
		sig.setBaseCharacterSet(baseChars);
		sig.setFirstIdentifierBase("1");
		sig.setIdentifierType(Context.getPatientService().getPatientIdentifierType(1));
		IdentifierSource source = iss.saveIdentifierSource(sig);
		
		Assert.assertNotNull(source.getId());
		IdentifierSource s = iss.getIdentifierSource(source.getId());
		Assert.assertEquals(s.getClass(), EgsIdentifierGenerator.class);
		Assert.assertEquals(s.getName(), name);
		Assert.assertEquals(((EgsIdentifierGenerator) s).getBaseCharacterSet(), baseChars);
	}

	@Test
    public void getIdentifierSourceByUuid_shouldGetSource() {
        IdentifierSource identifierSource = iss.getIdentifierSourceByUuid("0d47284f-9e9b-4a81-a88b-8bb42bc0a901");
        Assert.assertEquals(1, identifierSource.getId().intValue());
    }

    @Test
    public void getAutoGenerationOptionByPatientIdentifier_shouldGetAutoGenerationOptionByIdentifier() {
        PatientIdentifierType patientIdentifierType = patientService.getPatientIdentifierType(1);
        AutoGenerationOption autoGenerationOption = iss.getAutoGenerationOption(patientIdentifierType);
        Assert.assertEquals(1, autoGenerationOption.getId().intValue());
    }

    @Test
    public void getAutoGenerationOptionByPatientIdentifierAndLocation_shouldGetAutoGenerationOptionByIdentifier() {
        PatientIdentifierType patientIdentifierType = patientService.getPatientIdentifierType(1);
        Location location = locationService.getLocation(1);
        AutoGenerationOption autoGenerationOption = iss.getAutoGenerationOption(patientIdentifierType, location);
        Assert.assertEquals(1, autoGenerationOption.getId().intValue());
    }

    @Test
    public void getAutoGenerationOptionByPatientIdentifierAndLocation_shouldReturnNullIfNoOptionForLocation() {
        PatientIdentifierType patientIdentifierType = patientService.getPatientIdentifierType(2);
        Location location = locationService.getLocation(4);
        AutoGenerationOption autoGenerationOption = iss.getAutoGenerationOption(patientIdentifierType, location);
        Assert.assertNull(autoGenerationOption);
    }

    @Test(expected = NonUniqueResultException.class)
    public void getAutoGenerationOptionByPatientIdentifierAndLocation_shouldFailWhenTryingToFetchOptionByJustPatientIdentifierIfConfiguredByLocation() {
        PatientIdentifierType patientIdentifierType = patientService.getPatientIdentifierType(2);
        iss.getAutoGenerationOption(patientIdentifierType);
    }

    @Test
    public void getAutoGenerationOptionByPatientIdentifierAndLocation_shouldReturnOptionsNotConfiguredByLocation() {
        PatientIdentifierType patientIdentifierType = patientService.getPatientIdentifierType(1);
        Location location = locationService.getLocation(4);
        AutoGenerationOption autoGenerationOption = iss.getAutoGenerationOption(patientIdentifierType, location);
        Assert.assertEquals(1, autoGenerationOption.getId().intValue());
    }

    @Test
    public void getAutoGenerationOptionById_shouldFetchAutoGenerationOptionByPrimaryKey() {
        AutoGenerationOption option = iss.getAutoGenerationOption(1);
        Assert.assertEquals(1, option.getId().intValue());
    }
    
    private class BaseIdentifierSourceServiceStub extends BaseIdentifierSourceService {

        // we need to override the functionality to get and set sequential values since we are now
        // bypassing Hibernate and going directly to the DB to do this

        private long sequenceValue = 6;

        @Override
        public void saveSequenceValue(EgsIdentifierGenerator seq, long sequenceValue) {
            this.sequenceValue = sequenceValue;
        }

        @Override
        public Long getSequenceValue(EgsIdentifierGenerator seq) {
            return sequenceValue;
        }

    }
}
