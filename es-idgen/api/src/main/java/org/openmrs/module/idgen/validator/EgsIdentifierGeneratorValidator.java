package org.openmrs.module.idgen.validator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.module.idgen.EgsIdentifierGenerator;
import org.openmrs.patient.IdentifierValidator;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

/**
 * Validates a EgsIdentifierGeneratorValidator
 */
public class EgsIdentifierGeneratorValidator extends IdentifierSourceValidator {
	protected static Log log = LogFactory.getLog(EgsIdentifierGeneratorValidator.class);
	
	@Override
	public boolean supports(Class<?> arg0) {
		return EgsIdentifierGeneratorValidator.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		super.validate(obj, errors);
		
		EgsIdentifierGenerator source = (EgsIdentifierGenerator)obj;
		
		// FirstIdentifierBase is required
		if (!StringUtils.hasText(source.getFirstIdentifierBase())) {
			errors.reject("First Identifier Base is required");
		}
		
		if (source.getIdentifierType() == null) {
			errors.reject("Identifier Type is required");
			
		} else {
			String prefix = (source.getPrefix() == null ? "" : source.getPrefix());
			String suffix = (source.getSuffix() == null ? "" : source.getSuffix());
			String firstId = prefix + source.getFirstIdentifierBase() + suffix;
			
			if (StringUtils.hasText(source.getIdentifierType().getValidator())) {
				try {
					Class<?> validatorClass = Context.loadClass(source.getIdentifierType().getValidator());
					IdentifierValidator v = (IdentifierValidator) validatorClass.newInstance();
					firstId = v.getValidIdentifier(firstId);
				
				} catch (Exception e) {
					log.error("Error loading validator class " + source.getIdentifierType().getValidator(), e);
					errors.reject("Validator named " + source.getIdentifierType().getValidator() + " cannot be loaded");
				}
			}
			
			if (source.getMinLength() != null && source.getMinLength() > 0) {
				if (source.getMinLength() > firstId.length()) {
					errors.reject("Invalid configuration. First identifier generated would be '" + firstId + "' which is shorter than minimum length of " + source.getMinLength());
				}
			}
			
			if (source.getMaxLength() != null && source.getMaxLength() > 0) {
				if (source.getMaxLength() < firstId.length()) {
					errors.reject("Invalid configuration. First identifier generated would be '" + firstId + "' which exceeds maximum length of " + source.getMaxLength());
				}
			}
		}
	}
}
