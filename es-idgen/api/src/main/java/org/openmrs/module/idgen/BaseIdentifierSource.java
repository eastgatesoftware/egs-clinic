package org.openmrs.module.idgen;

import java.util.Date;

import org.openmrs.PatientIdentifierType;
import org.openmrs.User;

/**
 * An base implementation which provides all common property access
 */
public abstract class BaseIdentifierSource extends IdentifierSource {

	//***** PROPERTIES *****
	
	private Integer id;
	private String uuid;
	private String name;
	private String description;
	private PatientIdentifierType identifierType;
	private User creator;
	private Date dateCreated;
	private User changedBy;
	private Date dateChanged;
	private Boolean retired = Boolean.FALSE;
	private User retiredBy;
	private Date dateRetired;
	private String retireReason;
	
	public BaseIdentifierSource() {}
	
	//***** INSTANCE METHODS *****

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = prime + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (!(obj instanceof IdentifierSource))
			return false;
		
		BaseIdentifierSource other = (BaseIdentifierSource) obj;
		if (id == null) {
			if (other.id != null)
				return false;
			
		} else if (!id.equals(other.id))
			return false;
		
		if (uuid == null) {
			if (other.uuid != null)
				return false;
			
		} else if (!uuid.equals(other.uuid))
			return false;
		
		return true;
	}
	
	@Override
	public String toString() {
		return "BaseIdentifierSource [id=" + getId() 
				+ ", uuid=" + getUuid() 
				+ ", name=" + getName() 
				+ ", description=" + getDescription() 
				+ ", identifierType=" + getIdentifierType() 
				+ ", creator=" + getCreator() 
				+ "]";
	}
	
	//***** PROPERTY ACCESS ******

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public PatientIdentifierType getIdentifierType() {
		return identifierType;
	}

	public void setIdentifierType(PatientIdentifierType identifierType) {
		this.identifierType = identifierType;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public User getChangedBy() {
		return changedBy;
	}

	public void setChangedBy(User changedBy) {
		this.changedBy = changedBy;
	}

	public Date getDateChanged() {
		return dateChanged;
	}

	public void setDateChanged(Date dateChanged) {
		this.dateChanged = dateChanged;
	}

	public Boolean getRetired() {
		return retired;
	}

	public void setRetired(Boolean retired) {
		this.retired = retired;
	}

	public User getRetiredBy() {
		return retiredBy;
	}

	public void setRetiredBy(User retiredBy) {
		this.retiredBy = retiredBy;
	}

	public Date getDateRetired() {
		return dateRetired;
	}

	public void setDateRetired(Date dateRetired) {
		this.dateRetired = dateRetired;
	}

	public String getRetireReason() {
		return retireReason;
	}

	public void setRetireReason(String retireReason) {
		this.retireReason = retireReason;
	}
}
