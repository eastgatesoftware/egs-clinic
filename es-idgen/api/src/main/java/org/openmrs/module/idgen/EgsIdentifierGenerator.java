package org.openmrs.module.idgen;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.openmrs.api.context.Context;
import org.openmrs.patient.IdentifierValidator;

public class EgsIdentifierGenerator extends BaseIdentifierSource {
	
	//***** PROPERTIES *****

    private String prefix; // Optional prefix
    private String suffix; // Optional suffix
    private String firstIdentifierBase; // First identifier to start at
	private Integer minLength; // If > 0, will always return identifiers with a minimum of this length
	private Integer maxLength; // If > 0, will always return identifiers no longer than this length
    private String baseCharacterSet; // Enables configuration in appropriate Base
	
    //***** INSTANCE METHODS *****
    
    /**
     * Returns a boolean indicating whether this generator has already started producing identifiers
     */
    public boolean isInitialized() {
    	return false;
    }
    
    /**
     * Returns a new identifier for the given seed.  This does not change the state of the source
     * @param seed the seed to use for generation of the identifier
     * @return a new identifier for the given seed
	 * @should generate an identifier within minLength and maxLength bounds
	 * @should throw an error if generated identifier is shorter than minLength
	 * @should throw an error if generated identifier is longer than maxLength
     */
    public String getIdentifierForSeed(long seed) {
    	// Convert the next sequence integer into a String with the appropriate Base characters
    	int seqLength = firstIdentifierBase == null ? 1 : firstIdentifierBase.length();

    	String identifier = IdgenUtil.convertToBase(seed, baseCharacterSet.toCharArray(), seqLength);

    	// Add optional prefix and suffix
    	identifier = (prefix == null ? identifier : prefix + identifier);
    	identifier = (suffix == null ? identifier : identifier + suffix);

    	// Add check-digit, if required
    	if (getIdentifierType() != null && StringUtils.isNotEmpty(getIdentifierType().getValidator())) {
    	    try {
    		    Class<?> c = Context.loadClass(getIdentifierType().getValidator());
    		    IdentifierValidator v = (IdentifierValidator)c.newInstance();
    		    identifier = v.getValidIdentifier(identifier);
    	    }
    	    catch (Exception e) {
    	    	throw new RuntimeException("Error generating check digit with " + getIdentifierType().getValidator(), e);
    	    }
    	}

    	if (this.minLength != null && this.minLength > 0) {
    		if (identifier.length() < this.minLength) {
    			throw new RuntimeException("Invalid configuration for IdentifierSource. Length minimum set to " + this.minLength + " but generated " + identifier);
    		}
    	}

    	if (this.maxLength != null && this.maxLength > 0) {
    		if (identifier.length() > this.maxLength) {
    			throw new RuntimeException("Invalid configuration for IdentifierSource. Length maximum set to " + this.maxLength + " but generated " + identifier);
    		}
    	}

    	return identifier;
    }
    
    /**
     * Returns a new identifier for the given seed.  This does not change the state of the source
     * @param seed the seed to use for generation of the identifier
     * @return a new identifier for the given seed
	 * @should generate an identifier within minLength and maxLength bounds
	 * @should throw an error if generated identifier is shorter than minLength
	 * @should throw an error if generated identifier is longer than maxLength
     */
    public String getIdentifierForSeedIncludeBirthDate(long seed, Date birthDate) {
    	
    	// Convert the next sequence integer into a String with the appropriate Base characters
    	int seqLength = firstIdentifierBase == null ? 1 : firstIdentifierBase.length();

    	String identifier = IdgenUtil.convertToBase(seed, baseCharacterSet.toCharArray(), seqLength);

    	// Add the last 2 digits of the year of birth
    	if (birthDate != null) {
    		@SuppressWarnings("deprecation")
    		String year = String.valueOf(birthDate.getYear());
			String last2DigitOfYear = year.substring(year.length() - 2);
			identifier = last2DigitOfYear + identifier;
    	}
    		
    	// Add optional prefix and suffix
    	identifier = (prefix == null ? identifier : prefix + identifier);
    	identifier = (suffix == null ? identifier : identifier + suffix);

    	// Add check-digit, if required
    	if (getIdentifierType() != null && StringUtils.isNotEmpty(getIdentifierType().getValidator())) {
    	    try {
    		    Class<?> c = Context.loadClass(getIdentifierType().getValidator());
    		    IdentifierValidator v = (IdentifierValidator)c.newInstance();
    		    identifier = v.getValidIdentifier(identifier);
    	    }
    	    catch (Exception e) {
    	    	throw new RuntimeException("Error generating check digit with " + getIdentifierType().getValidator(), e);
    	    }
    	}

    	if (this.minLength != null && this.minLength > 0) {
    		if (identifier.length() < this.minLength) {
    			throw new RuntimeException("Invalid configuration for IdentifierSource. Length minimum set to " + this.minLength + " but generated " + identifier);
    		}
    	}

    	if (this.maxLength != null && this.maxLength > 0) {
    		if (identifier.length() > this.maxLength) {
    			throw new RuntimeException("Invalid configuration for IdentifierSource. Length maximum set to " + this.maxLength + " but generated " + identifier);
    		}
    	}

    	return identifier;
    }
    
    //***** PROPERTY ACCESS ******
    
    public String getPrefix() {
		return prefix;
	}
    
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getFirstIdentifierBase() {
		return firstIdentifierBase;
	}

	public void setFirstIdentifierBase(String firstIdentifierBase) {
		this.firstIdentifierBase = firstIdentifierBase;
	}

	public Integer getMinLength() {
		return minLength;
	}

	public void setMinLength(Integer minLength) {
		this.minLength = minLength;
	}

	public Integer getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}

	public String getBaseCharacterSet() {
		return baseCharacterSet;
	}

	public void setBaseCharacterSet(String baseCharacterSet) {
		this.baseCharacterSet = baseCharacterSet;
	}
}
