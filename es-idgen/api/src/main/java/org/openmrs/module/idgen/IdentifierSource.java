package org.openmrs.module.idgen;

import java.util.Date;

import org.openmrs.PatientIdentifierType;
import org.openmrs.User;

/**
 * An IdentifierSource is a construct which can supply identifiers of a particular type
 */
public abstract class IdentifierSource {
	public abstract Integer getId();

	public abstract void setId(Integer id);

	public abstract String getUuid();

	public abstract void setUuid(String uuid);

	public abstract String getName();

	public abstract void setName(String name);

	public abstract String getDescription();

	public abstract void setDescription(String description);

	public abstract PatientIdentifierType getIdentifierType();

	public abstract void setIdentifierType(PatientIdentifierType identifierType);

	public abstract User getCreator();

	public abstract void setCreator(User creator);

	public abstract Date getDateCreated();

	public abstract void setDateCreated(Date dateCreated);

	public abstract User getChangedBy();

	public abstract void setChangedBy(User changedBy);

	public abstract Date getDateChanged();

	public abstract void setDateChanged(Date dateChanged);

	public abstract Boolean getRetired();

	public abstract void setRetired(Boolean retired);

	public abstract User getRetiredBy();

	public abstract void setRetiredBy(User retiredBy);

	public abstract Date getDateRetired();

	public abstract void setDateRetired(Date dateRetired);

	public abstract String getRetireReason();

	public abstract void setRetireReason(String retireReason);
}
