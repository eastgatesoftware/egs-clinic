package org.openmrs.module.idgen;

import org.openmrs.Location;
import org.openmrs.PatientIdentifierType;

public class AutoGenerationOption {
	
	//***** PROPERTIES *****
	
    private Integer id;
    private PatientIdentifierType identifierType;
    private Location location;
    private IdentifierSource source;
    private boolean manualEntryEnabled = true;
    private boolean automaticGenerationEnabled = false;
    
    //***** CONSTRUCTORS *****
    
    public AutoGenerationOption() { }
    
    public AutoGenerationOption(PatientIdentifierType identifierType) {
    	this();
    	this.identifierType = identifierType;
    }
    
    public AutoGenerationOption(PatientIdentifierType identifierType, IdentifierSource source, 
								boolean manualEntryEnabled, boolean automaticGenerationEnabled) {
		this(identifierType);
		this.source = source;
		this.manualEntryEnabled = manualEntryEnabled;
		this.automaticGenerationEnabled = automaticGenerationEnabled;
	}
    
    //***** INSTANCE METHODS *****
    
    @Override
	public int hashCode() {
		final int prime = 31;
		return prime + ((id == null) ? 0 : id.hashCode()); // 31 * i == (i << 5) - i
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (!(obj instanceof AutoGenerationOption))
			return false;
		
		AutoGenerationOption other = (AutoGenerationOption) obj;
		if (id == null) {
			if (other.id != null)
				return false;
			
		} else if (!id.equals(other.id))
			return false;
		
		return true;
	}
	
	@Override
	public String toString() {
		return "AutoGenerationOption [id=" + getId() 
				+ ", identifierType=" + getIdentifierType() 
				+ ", location=" + getLocation() 
				+ ", source=" + getSource()
				+ ", manualEntryEnabled=" + isManualEntryEnabled()
				+ ", automaticGenerationEnabled=" + isAutomaticGenerationEnabled()
				+ "]";
	}
	
    //***** PROPERTY ACCESS ******

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public PatientIdentifierType getIdentifierType() {
		return identifierType;
	}
	
	public void setIdentifierType(PatientIdentifierType identifierType) {
		this.identifierType = identifierType;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public IdentifierSource getSource() {
		return source;
	}

	public void setSource(IdentifierSource source) {
		this.source = source;
	}

	public boolean isManualEntryEnabled() {
		return manualEntryEnabled;
	}

	public void setManualEntryEnabled(boolean manualEntryEnabled) {
		this.manualEntryEnabled = manualEntryEnabled;
	}

	public boolean isAutomaticGenerationEnabled() {
		return automaticGenerationEnabled;
	}

	public void setAutomaticGenerationEnabled(boolean automaticGenerationEnabled) {
		this.automaticGenerationEnabled = automaticGenerationEnabled;
	}
}
