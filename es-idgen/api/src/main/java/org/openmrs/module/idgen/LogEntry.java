package org.openmrs.module.idgen;

import java.util.Date;

import org.openmrs.User;

public class LogEntry {
	
	//***** PROPERTIES *****
	
    private Integer id;
    private IdentifierSource source;
    private String identifier;
    private Date dateGenerated;
    private User generatedBy;
    private String comment;
	
    //***** CONSTRUCTORS *****
    
    public LogEntry() { }
    
    public LogEntry(IdentifierSource source, String identifier, Date dateGenerated, User generatedBy, String comment) { 
    	this.source = source;
    	this.identifier = identifier;
    	this.dateGenerated = dateGenerated;
    	this.generatedBy = generatedBy;
    	this.comment = comment;
    }
    
    //***** INSTANCE METHODS *****
 
    @Override
	public int hashCode() {
		final int prime = 31;
		return prime + ((id == null) ? 0 : id.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (!(obj instanceof LogEntry))
			return false;
		
		LogEntry other = (LogEntry) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		
		} else if (!id.equals(other.id))
			return false;
		
		return true;
	}

    //***** PROPERTY ACCESS *****
    
    public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public IdentifierSource getSource() {
		return source;
	}

	public void setSource(IdentifierSource source) {
		this.source = source;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Date getDateGenerated() {
		return dateGenerated;
	}

	public void setDateGenerated(Date dateGenerated) {
		this.dateGenerated = dateGenerated;
	}

	public User getGeneratedBy() {
		return generatedBy;
	}

	public void setGeneratedBy(User generatedBy) {
		this.generatedBy = generatedBy;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
    
}
