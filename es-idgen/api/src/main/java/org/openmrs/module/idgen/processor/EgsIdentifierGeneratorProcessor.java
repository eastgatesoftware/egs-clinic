package org.openmrs.module.idgen.processor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openmrs.module.idgen.EgsIdentifierGenerator;
import org.openmrs.module.idgen.IdentifierSource;
import org.openmrs.module.idgen.IdgenUtil;
import org.openmrs.module.idgen.api.IdentifierSourceService;

/**
 * Evaluates a EgsIdentifierSource
 */
public class EgsIdentifierGeneratorProcessor implements
		IdentifierSourceProcessor {

	// ***** PROPERTIES *****

	private IdentifierSourceService identifierSourceService;

	// ***** INSTANCE METHODS *****

	@Override
	public synchronized List<String> getIdentifiers(IdentifierSource source,
			int batchSize) {

		EgsIdentifierGenerator generator = (EgsIdentifierGenerator) source;
		long sequenceValue = identifierSourceService
				.getSequenceValue(generator);
		if (sequenceValue < 0) {
			if (generator.getFirstIdentifierBase() != null) {
				sequenceValue = IdgenUtil.convertFromBase(generator
						.getFirstIdentifierBase(), generator
						.getBaseCharacterSet().toCharArray());

			} else {
				sequenceValue = 1;
			}
		}

		List<String> identifiers = new ArrayList<String>();
		for (int i = 0; i < batchSize;) {
			String val = generator.getIdentifierForSeed(sequenceValue);
			identifiers.add(val);
			i++;
			sequenceValue++;
		}

		identifierSourceService.saveSequenceValue(generator, sequenceValue);
		return identifiers;
	}

	@Override
	public String getIdentifierIncludeBirthDate(IdentifierSource source,
			Date birthDate) {

		EgsIdentifierGenerator generator = (EgsIdentifierGenerator) source;
		long sequenceValue = identifierSourceService
				.getSequenceValue(generator);
		if (sequenceValue < 0) {
			if (generator.getFirstIdentifierBase() != null) {
				sequenceValue = IdgenUtil.convertFromBase(generator
						.getFirstIdentifierBase(), generator
						.getBaseCharacterSet().toCharArray());

			} else {
				sequenceValue = 1;
			}
		}

		String identifier = generator.getIdentifierForSeedIncludeBirthDate(sequenceValue,
				birthDate);
		sequenceValue++;

		identifierSourceService.saveSequenceValue(generator, sequenceValue);
		return identifier;
	}

	// ***** PROPERTY ACCESS ******

	public IdentifierSourceService getIdentifierSourceService() {
		return identifierSourceService;
	}

	public void setIdentifierSourceService(
			IdentifierSourceService identifierSourceService) {
		this.identifierSourceService = identifierSourceService;
	}
}
