package org.openmrs.module.idgen.validator;

import org.openmrs.module.idgen.IdentifierSource;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Validates an IdentifierSource
 */
public class IdentifierSourceValidator implements Validator  {

	@Override
	public boolean supports(Class<?> arg0) {
		return IdentifierSource.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		IdentifierSource source = (IdentifierSource)obj;
		
		// Name is required
		if (!StringUtils.hasText(source.getName())) {
			errors.reject("Name of IdentifierSource is required");
		}
	}
}
