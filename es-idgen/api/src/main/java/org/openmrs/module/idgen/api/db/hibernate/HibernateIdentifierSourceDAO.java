/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.idgen.api.db.hibernate;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openmrs.Location;
import org.openmrs.PatientIdentifierType;
import org.openmrs.api.APIException;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.idgen.AutoGenerationOption;
import org.openmrs.module.idgen.EgsIdentifierGenerator;
import org.openmrs.module.idgen.IdentifierSource;
import org.openmrs.module.idgen.LogEntry;
import org.openmrs.module.idgen.api.db.IdentifierSourceDAO;
import org.springframework.transaction.annotation.Transactional;

/**
 *  Hibernate Implementation of the IdentifierSourceDAO Interface
 */
public class HibernateIdentifierSourceDAO implements IdentifierSourceDAO {
	
	protected final Log log = LogFactory.getLog(this.getClass());
	
	//***** PROPERTIES *****
	
	private SessionFactory sessionFactory;
	
	
	//***** INSTANCE METHODS *****

	@Transactional(readOnly=true)
	public IdentifierSource getIdentifierSource(Integer id) throws DAOException {
		return (IdentifierSource) sessionFactory.getCurrentSession().get(IdentifierSource.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<IdentifierSource> getAllIdentifierSources(boolean includeRetired) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(IdentifierSource.class);
		if (!includeRetired) {
			criteria.add(Expression.like("retired", false));
		}
		
		criteria.addOrder(Order.asc("name"));
		return criteria.list();
	}

	@Transactional(readOnly=true)
	public IdentifierSource saveIdentifierSource(IdentifierSource identifierSource) throws DAOException {
		sessionFactory.getCurrentSession().saveOrUpdate(identifierSource);
		return identifierSource;
	}

	@Transactional(readOnly=true)
    public void purgeIdentifierSource(IdentifierSource identifierSource) throws DAOException {
		sessionFactory.getCurrentSession().delete(identifierSource);
	}

	@Transactional(readOnly=true)
    public AutoGenerationOption getAutoGenerationOption(Integer autoGenerationOptionId) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AutoGenerationOption.class);
		criteria.add(Expression.eq("id", autoGenerationOptionId));
		return (AutoGenerationOption) criteria.uniqueResult();
	}

	@Transactional(readOnly=true)
    public AutoGenerationOption getAutoGenerationOption(PatientIdentifierType type, Location location) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AutoGenerationOption.class);
		criteria.add(Expression.eq("identifierType", type));
		criteria.add(Restrictions.or(Expression.eq("location", location), Expression.isNull("location")));
		return (AutoGenerationOption) criteria.uniqueResult();
	}

	@Transactional(readOnly=true)
    public AutoGenerationOption getAutoGenerationOption(PatientIdentifierType type) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AutoGenerationOption.class);
		criteria.add(Expression.eq("identifierType", type));
		return (AutoGenerationOption) criteria.uniqueResult();
	}

	@Transactional(readOnly=true)
    public AutoGenerationOption saveAutoGenerationOption(AutoGenerationOption option) throws DAOException {
		sessionFactory.getCurrentSession().saveOrUpdate(option);
		return option;
	}

	@Transactional(readOnly=true)
    public void purgeAutoGenerationOption(AutoGenerationOption option) throws DAOException {
		sessionFactory.getCurrentSession().delete(option);
	}

	@Override
	public LogEntry saveLogEntry(LogEntry logEntry) throws DAOException {
		sessionFactory.getCurrentSession().saveOrUpdate(logEntry);
		return logEntry;
	}

	@Override
	public IdentifierSource getIdentifierSourceByUuid(String uuid) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(IdentifierSource.class);
		criteria.add(Restrictions.eq("uuid", uuid));
		return (IdentifierSource) criteria.uniqueResult();
	}
	
	//***** PROPERTY ACCESS *****
	
	public void setSessionFactory(SessionFactory sessionFactory) {
	    this.sessionFactory = sessionFactory;
	}
	    
	public SessionFactory getSessionFactory() {
	    return sessionFactory;
	}

	@Override
	public void saveSequenceValue(EgsIdentifierGenerator generator, long sequenceValue) {
		int updated = sessionFactory.getCurrentSession()
					.createSQLQuery("update idgen_seq_id_gen set next_sequence_value = :val where id = :id")
					.setParameter("val", sequenceValue)
					.setParameter("id", generator.getId())
					.executeUpdate();
		
		if (updated != 1)
			throw new APIException("Expected to update 1 row but updated " + updated + " rows instead");
		
	}

	@Override
	public Long getSequenceValue(EgsIdentifierGenerator generator) {
		Number value = (Number) sessionFactory.getCurrentSession()
						.createSQLQuery("select next_sequence_value from idgen_seq_id_gen where id = :id")
						.setParameter("id", generator.getId())
						.uniqueResult();
		
		return value == null ? null : value.longValue();
	}
	
	public void refreshIdentifierSource(IdentifierSource source) {
        sessionFactory.getCurrentSession().refresh(source);
    }
}