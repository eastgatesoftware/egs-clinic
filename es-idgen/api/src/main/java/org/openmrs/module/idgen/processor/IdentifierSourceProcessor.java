package org.openmrs.module.idgen.processor;

import java.util.Date;
import java.util.List;

import org.openmrs.module.idgen.IdentifierSource;

/**
 * The interface provides the functionality for using an IdentifierSource
 */
public interface IdentifierSourceProcessor {
	
	/**
     * Return List of new identifiers from this source
     * @param batchSize the number of new identifiers to return
     * @return a list of new identifiers from this source
     */
    public List<String> getIdentifiers(IdentifierSource source, int batchSize);
    
    public String getIdentifierIncludeBirthDate(IdentifierSource source, Date birthDate);
}
