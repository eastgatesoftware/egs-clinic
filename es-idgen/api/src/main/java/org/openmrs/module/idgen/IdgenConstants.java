package org.openmrs.module.idgen;

/**
 * Constants useful for this module
 */
public class IdgenConstants {
	
	/**
	 * Privilege which grants users permission to manage identifier sources
	 */
	public static final String PRIV_MANAGE_IDENTIFIER_SOURCES = "Manage Identifier Sources";
	
	/**
	 * Privilege which grants users permission to manage auto-generation options
	 */
	public static final String PRIV_MANAGE_AUTOGENERATION_OPTIONS = "Manage Auto Generation Options";

	/**
	 * Privilege which grants users permission to generate a batch of identifiers to a file for offline use
	 */
	public static final String PRIV_GENERATE_BATCH_OF_IDENTIFIERS = "Generate Batch of Identifiers";
	
}
