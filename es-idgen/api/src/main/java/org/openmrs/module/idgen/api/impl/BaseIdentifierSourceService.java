package org.openmrs.module.idgen.api.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Location;
import org.openmrs.PatientIdentifierType;
import org.openmrs.User;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.idgen.AutoGenerationOption;
import org.openmrs.module.idgen.EgsIdentifierGenerator;
import org.openmrs.module.idgen.IdentifierSource;
import org.openmrs.module.idgen.LogEntry;
import org.openmrs.module.idgen.api.IdentifierSourceService;
import org.openmrs.module.idgen.processor.IdentifierSourceProcessor;
import org.openmrs.module.idgen.api.db.IdentifierSourceDAO;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class BaseIdentifierSourceService extends BaseOpenmrsService implements IdentifierSourceService {

	protected Log log = LogFactory.getLog(getClass());
	
	//***** PROPERTIES *****
	
	private static Map<Class<? extends IdentifierSource>, IdentifierSourceProcessor> processors = null;
	
	private ConcurrentHashMap<Integer, Object> syncLocks = new ConcurrentHashMap<Integer, Object>();
	
	private IdentifierSourceDAO dao = null;
	
	//***** INSTANCE METHODS *****
	
	@Transactional(readOnly = true)
	public List<Class<? extends IdentifierSource>> getIdentifierSourceTypes() {
		List<Class<? extends IdentifierSource>> sourceTypes = new ArrayList<Class<? extends IdentifierSource>>();
		sourceTypes.add(EgsIdentifierGenerator.class);
		return sourceTypes;
	}

	@Transactional(readOnly = true)
	public IdentifierSource getIdentifierSource(Integer id) throws APIException {
		return dao.getIdentifierSource(id);
	}

	@Transactional(readOnly = true)
	public List<IdentifierSource> getAllIdentifierSources(boolean includeRetired)
			throws APIException {
		return dao.getAllIdentifierSources(includeRetired);
	}

	@Transactional(readOnly = true)
	public Map<PatientIdentifierType, List<IdentifierSource>> getIdentifierSourcesByType(boolean includeRetired) throws APIException {
		Map<PatientIdentifierType, List<IdentifierSource>> m = new LinkedHashMap<PatientIdentifierType, List<IdentifierSource>>();
		for (PatientIdentifierType t : Context.getPatientService().getAllPatientIdentifierTypes()) {
			m.put(t, new ArrayList<IdentifierSource>());
		}
		
		for (IdentifierSource s : getAllIdentifierSources(includeRetired)) {
			m.get(s.getIdentifierType()).add(s);
		}
		
		return m;
	}
	
	@Override
	public IdentifierSource saveIdentifierSource(IdentifierSource identifierSource) throws APIException {
		if (identifierSource.getName() == null) {
			throw new APIException("Identifier Source name is required");
		}
		
		if (identifierSource.getUuid() == null) {
			identifierSource.setUuid(UUID.randomUUID().toString());
		}
		
		User u = Context.getAuthenticatedUser();
		Date today = new Date();
		if (identifierSource.getDateCreated() == null) {
			identifierSource.setDateCreated(today);
		}
		
		if (identifierSource.getCreator() == null) {
			identifierSource.setCreator(u);
		}
		
		if (identifierSource.getId() != null) {
			identifierSource.setChangedBy(u);
			identifierSource.setDateChanged(today);
		}
		
		return dao.saveIdentifierSource(identifierSource);
	}

	@Transactional
	public void purgeIdentifierSource(IdentifierSource identifierSource) {
		dao.purgeIdentifierSource(identifierSource);
	}

	@Override
	public String generateIdentifier(PatientIdentifierType type, String comment) {
		AutoGenerationOption option = getAutoGenerationOption(type);
		
		if (option != null && option.isAutomaticGenerationEnabled()) {
			return generateIdentifier(option.getSource(), comment);
		} else {
			return  null;
		}
	}

	@Override
	public String generateIdentifier(PatientIdentifierType type, Location location, String comment) {
		AutoGenerationOption option = getAutoGenerationOption(type, location);
		
		if (option != null && option.isAutomaticGenerationEnabled()) {
			return generateIdentifier(option.getSource(), comment);
		} else {
			return null;
		}
	}

	@Override
	public String generateIdentifier(IdentifierSource source, Date birthDate, String comment) throws APIException {
		String identifier = generateIdentifierIncludeBirthDate(source, birthDate, comment);
		if (identifier == null) {
			throw new RuntimeException("Generate identifier method did not return only one identifier");
		}
		
		return identifier;
	}
	
	@Override
	public String generateIdentifier(IdentifierSource source, String comment) throws APIException {
		List<String> list = generateIdentifiers(source, 1, comment);
		if (list == null || list.size() != 1) {
			throw new RuntimeException("Generate identifier method did not return only one identifier");
		}
		
		return list.get(0);
	}

	@Transactional(readOnly=true)
	public IdentifierSourceProcessor getProcessor(IdentifierSource source) {
		return getProcessors().get(source.getClass());
	}

	@Override
	public void registerProcessor(Class<? extends IdentifierSource> type, IdentifierSourceProcessor processorToRegister) throws APIException {
		getProcessors().put(type, processorToRegister);
	}

	@Transactional(readOnly=true)
	@Override
	public AutoGenerationOption getAutoGenerationOption(Integer autoGenerationOptionId) throws APIException {
		return dao.getAutoGenerationOption(autoGenerationOptionId);
	}

	@Transactional(readOnly=true)
	@Override
	public AutoGenerationOption getAutoGenerationOption(PatientIdentifierType type, Location location) throws APIException {
		return dao.getAutoGenerationOption(type, location);
	}

	@Transactional(readOnly = true)
	@Override
	public AutoGenerationOption getAutoGenerationOption(PatientIdentifierType type) throws APIException {
		return dao.getAutoGenerationOption(type);
	}

	@Transactional
	@Override
	public AutoGenerationOption saveAutoGenerationOption(AutoGenerationOption option) throws APIException {
		return dao.saveAutoGenerationOption(option);
	}

	@Transactional
	@Override
	public void purgeAutoGenerationOption(AutoGenerationOption option) throws APIException {
		dao.purgeAutoGenerationOption(option);
	}

	@Override
	public List<PatientIdentifierType> getPatientIdentifierTypesByAutoGenerationOption(Boolean manualEntryEnabled, Boolean autoGenerationEnabled) {
		List<PatientIdentifierType> identifierTypes = new ArrayList<PatientIdentifierType>();
		
		for (PatientIdentifierType type : Context.getPatientService().getAllPatientIdentifierTypes()) {
			AutoGenerationOption option = getAutoGenerationOption(type);
			if (option != null && option.isManualEntryEnabled() == manualEntryEnabled.booleanValue() 
					&& option.isAutomaticGenerationEnabled() == autoGenerationEnabled.booleanValue()) {
				identifierTypes.add(type);
			}
		}
		return identifierTypes;
	}

	@Override
	public List<String> generateIdentifiers(IdentifierSource source, Integer batchSize, String comment) throws APIException {
		if (log.isDebugEnabled()) {
			log.debug("About to enter synchronize block for " + source.getName());
		}
		
		Object syncLock = getSyncLock(source.getId());
		synchronized (syncLock) {
			List<String> identifiers = Context.getService(IdentifierSourceService.class).generateIdentifiersInternal(source.getId(), batchSize, comment);
			dao.refreshIdentifierSource(source);
            return identifiers;
		}
	}
	
	@Override
	public String generateIdentifierIncludeBirthDate(IdentifierSource source, Date birthDate, String comment) throws APIException {
		if (log.isDebugEnabled()) {
			log.debug("About to enter synchronize block for " + source.getName());
		}
		
		Object syncLock = getSyncLock(source.getId());
		synchronized (syncLock) {
			String identifier = Context.getService(IdentifierSourceService.class).generateIdentifierInternalIncludeBirthDate(source.getId(), birthDate, comment);
			dao.refreshIdentifierSource(source);
            return identifier;
		}
	}
	
	private Object getSyncLock(Integer identifierSourceId) {
        
		// this method does not need to be synchronized, because putIfAbsent is atomic
        syncLocks.putIfAbsent(identifierSourceId, new Object());
        return syncLocks.get(identifierSourceId);
    }
	
	@Override
	public IdentifierSource getIdentifierSourceByUuid(String uuid) {
		return dao.getIdentifierSourceByUuid(uuid);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public List<String> generateIdentifiersInternal(Integer sourceId, Integer batchSize, String comment) {
		IdentifierSource source = getIdentifierSource(sourceId);
		IdentifierSourceProcessor processor = getProcessor(source);
		
		if (processor == null) {
			throw new APIException("No registered processor found for source: " + source);
		}
		
		List<String> identifiers = processor.getIdentifiers(source, batchSize);
		
		Date now = new Date();
		User currentUser = Context.getAuthenticatedUser();
		
		for (String s : identifiers) {
			LogEntry logEntry = new LogEntry(source, s, now, currentUser, comment);
			dao.saveLogEntry(logEntry);
		}
		
		return identifiers;
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public String generateIdentifierInternalIncludeBirthDate(Integer sourceId, Date birthDate, String comment) {
		IdentifierSource source = getIdentifierSource(sourceId);
		IdentifierSourceProcessor processor = getProcessor(source);
		
		if (processor == null) {
			throw new APIException("No registered processor found for source: " + source);
		}
		
		String identifier = processor.getIdentifierIncludeBirthDate(source, birthDate);
		
		Date now = new Date();
		User currentUser = Context.getAuthenticatedUser();
		
		LogEntry logEntry = new LogEntry(source, identifier, now, currentUser, comment);
		dao.saveLogEntry(logEntry);
		
		return identifier;
	}
	
    @Override
    public void saveSequenceValue(EgsIdentifierGenerator generator, long sequenceValue) {
    	dao.saveSequenceValue(generator, sequenceValue);
    }

    @Override
    public Long getSequenceValue(EgsIdentifierGenerator generator) {
        return dao.getSequenceValue(generator);
    }

	//***** PROPERTY ACCESS *****
	
	public IdentifierSourceDAO getDao() {
		return dao;
	}

	public void setDao(IdentifierSourceDAO dao) {
		this.dao = dao;
	}

	public Map<Class<? extends IdentifierSource>, IdentifierSourceProcessor> getProcessors() {
		if (processors == null)
			processors = new HashMap<Class<? extends IdentifierSource>, IdentifierSourceProcessor>();
		
		return processors;
	}

	public void setProcessors(Map<Class<? extends IdentifierSource>, IdentifierSourceProcessor> processorsToAdd) {
		if (processorsToAdd != null) {
			for (Map.Entry<Class<? extends IdentifierSource>, IdentifierSourceProcessor> entry : processorsToAdd.entrySet()) {
				getProcessors().put(entry.getKey(), entry.getValue());
			}
		}
	}

}
