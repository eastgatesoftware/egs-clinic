package org.openmrs.module.coreapps.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.api.context.ContextAuthenticationException;
import org.openmrs.module.coreapps.CoreAppsConstants;
import org.openmrs.module.coreapps.LoginForm;
import org.openmrs.module.coreapps.validator.LoginFormValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Processes requests to authenticate a user
 */
@Controller
public class LoginPageController {

	protected final Log log = LogFactory.getLog(getClass());

	@RequestMapping(value = CoreAppsConstants.OBSOLETE_LOGIN_URL)
	public String overrideLoginPage() {
		return CoreAppsConstants.FORWARD_LOGIN_URL;
	}

	@RequestMapping(value = CoreAppsConstants.LOGIN_URL, method = RequestMethod.GET)
	public String displayLogin(Model model) {
		model.addAttribute("loginModel", new LoginForm());
		
		return CoreAppsConstants.LOGIN_PAGE_LOCATION;
	}

	@RequestMapping(value = CoreAppsConstants.LOGIN_URL, method = RequestMethod.POST)
	public String executeLogin(@ModelAttribute("loginModel") LoginForm loginForm, BindingResult result) {

		// Validate form data
		LoginFormValidator validator = new LoginFormValidator();
		validator.validate(loginForm, result);
		if (result.hasErrors()) {
			return CoreAppsConstants.LOGIN_PAGE_LOCATION;
		}

		// Authenticate user
		try {
			Context.authenticate(loginForm.getUsername(),
					loginForm.getPassword());

			if (Context.isAuthenticated()) {
				if (log.isDebugEnabled()) {
					log.debug("User has successfully authenticated");
				}
			}
		} catch (ContextAuthenticationException e) {
			if (log.isDebugEnabled()) {
				log.debug("Failed to authenticate user");
			}
		}
		return CoreAppsConstants.REDIRECT_REGISTER_PATIENR_URL;
	}
}
