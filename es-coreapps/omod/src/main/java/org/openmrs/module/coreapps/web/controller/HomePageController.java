package org.openmrs.module.coreapps.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.module.coreapps.CoreAppsConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomePageController {

	protected final Log log = LogFactory.getLog(getClass());

	@RequestMapping(value = CoreAppsConstants.OBSOLETE_HOMEPAGE_URL)
	public String overrideHomePage() {
		return CoreAppsConstants.FORWARD_LOGIN_URL;
	}
}
