package org.openmrs.module.coreapps;

import com.mchange.util.AssertException;

public class CoreAppsConstants {
	
	private CoreAppsConstants() {
		throw new AssertException();
	}

	// Url
	public static final String OBSOLETE_HOMEPAGE_URL = "/index.htm";
	public static final String OBSOLETE_LOGIN_URL = "/login.htm";
	
	public static final String LOGIN_URL = "/module/coreapps/login.form";
	public static final String FORWARD_LOGIN_URL = "forward:/module/coreapps/login.form";
	
	public static final String FIND_PATIENT_URL = "/module/coreapps/findPatient.form";
	
	public static final String REDIRECT_REGISTER_PATIENR_URL = "redirect:/module/registrationapp/registerPatient.form";

	// Jsp file location
	public static final String FIND_PATIENT_PAGE_LOCATION = "/module/es-coreapps/pages/findPatient/findPatient";
	public static final String LOGIN_PAGE_LOCATION = "/module/es-coreapps/pages/login/login";


}
