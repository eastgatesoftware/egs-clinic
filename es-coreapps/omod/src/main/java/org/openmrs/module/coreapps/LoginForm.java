package org.openmrs.module.coreapps;

public class LoginForm {

	// ***** PROPERTIES *****

	private String username;
	private String password;

	public LoginForm() {

	}

	// ***** PROPERTY ACCESS ******

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
