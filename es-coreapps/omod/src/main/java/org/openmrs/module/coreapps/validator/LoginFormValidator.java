package org.openmrs.module.coreapps.validator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.APIException;
import org.openmrs.module.coreapps.LoginForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class LoginFormValidator implements Validator {
	protected final Log log = LogFactory.getLog(getClass());

	@Override
	public boolean supports(Class<?> clazz) {
		return LoginFormValidator.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		if (log.isDebugEnabled()) {
			log.debug(this.getClass().getName()
					+ ": Validating form data from the login form!");
		}

		LoginForm loginForm = (LoginForm) obj;
		if (loginForm == null) {
			throw new APIException("The Login form data cannot be null");
		}

		if (StringUtils.isBlank(loginForm.getUsername())) {
			errors.rejectValue("username", "login.username.required");
		}
		
		if (StringUtils.isBlank(loginForm.getPassword())) {
			errors.rejectValue("password", "login.password.required");
		}
	}
}
