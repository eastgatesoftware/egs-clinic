package org.openmrs.module.coreapps;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * The Model Object to be used for the register patient form.
 */
public class PatientsStub {

	// ***** PROPERTIES *****

	private List<PatientForm> listPatient = new LinkedList<PatientForm>();
	
	public PatientsStub() {
	    PatientForm p1 = new PatientForm();
        p1.setIdentifier("88000001");
	    p1.setFirstName("Chinh");
        p1.setLastName("Nguyen");
        p1.setGender("M");
        p1.setBirthDate(new Date());
        listPatient.add(p1);
        
        PatientForm p2 = new PatientForm();
        p2.setIdentifier("88000001");
        p2.setFirstName("Chinh");
        p2.setLastName("Nguyen");
        p2.setGender("M");
        p2.setBirthDate(new Date());
        listPatient.add(p2);
        
        PatientForm p3 = new PatientForm();
        p3.setIdentifier("88000001");
        p3.setFirstName("Chinh");
        p3.setLastName("Nguyen");
        p3.setGender("M");
        p3.setBirthDate(new Date());
        listPatient.add(p3);
        
        PatientForm p4 = new PatientForm();
        p4.setIdentifier("88000001");
        p4.setFirstName("Chinh");
        p4.setLastName("Nguyen");
        p4.setGender("M");
        p4.setBirthDate(new Date());
        listPatient.add(p4);
        
        this.setListPatient(listPatient);
        
    }

    public List<PatientForm> getListPatient() {
        return listPatient;
    }

    public void setListPatient(List<PatientForm> listPatient) {
        this.listPatient = listPatient;
    }
    
}
