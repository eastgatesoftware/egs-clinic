package org.openmrs.module.coreapps.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.module.coreapps.CoreAppsConstants;
import org.openmrs.module.coreapps.PatientsStub;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FindPatientPageController {
	protected final Log log = LogFactory.getLog(getClass());

	static PatientsStub patients = new PatientsStub();
	@RequestMapping(value = CoreAppsConstants.FIND_PATIENT_URL + "?skip={skip}&take={take}", method = RequestMethod.GET)
	public @ResponseBody String byParameter(@PathVariable String skip,
			@PathVariable String take, Model model) {
		
		model.addAttribute("user", Context.getAuthenticatedUser());
		model.addAttribute("patients", patients.getListPatient());
		return CoreAppsConstants.FIND_PATIENT_PAGE_LOCATION;

	}

	@RequestMapping(value = CoreAppsConstants.FIND_PATIENT_URL, method = RequestMethod.GET)
	public String getPatients(Model model) {
		model.addAttribute("user", Context.getAuthenticatedUser());
		model.addAttribute("patients", patients.getListPatient());
		
		return CoreAppsConstants.FIND_PATIENT_PAGE_LOCATION;
	}
}
