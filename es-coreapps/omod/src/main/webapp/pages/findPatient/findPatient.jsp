<%@ include file="localHeader.jsp"%>
<%@ include file="/WEB-INF/es-template/mainLeftMenu.jsp"%>

<div xmlns="http://www.w3.org/1999/xhtml" xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:jsp="http://java.sun.com/JSP/Page"
    xmlns:spring="http://www.springframework.org/tags" xmlns:openmrs="urn:jsptld:/WEB-INF/taglibs/openmrs.tld"
>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header find-patient-header">
            <h1 class="text-light-blue">Find Patient</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <section class="content find-patient-content">
                <div class="row">
                    <table id="patient-search-results-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Identifier</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Age</th>
                                <th>Birthdate</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="patient" items="${patients}">
                                <tr>
                                    <td>${patient.identifier}</td>
                                    <td>${patient.firstName}</td>
                                    <td>${patient.gender}</td>
                                    <td>28</td>
                                    <td>1/1/1988</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="dataTables_paginate paging_simple_numbers" id="patient-search-results-table_paginate">
                    <ul class="pagination">
                        <li class="paginate_button active"><a href="#" data-dt-idx="1" tabindex="0">1</a></li>
                        <li class="paginate_button "><a href="#" data-dt-idx="2" tabindex="0">2</a></li>
                    </ul>
                </div>
            </section>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
<%@ include file="localFooter.jsp"%>