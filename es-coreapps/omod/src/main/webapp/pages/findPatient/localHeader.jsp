<%@ include file="/WEB-INF/es-template/include.jsp"%>
<%@ include file="/WEB-INF/es-template/head.jsp"%>

<openmrs:message var="pageTitle" code="es-coreapps.findpatient.title" scope="page"/>
<openmrs:htmlInclude file="/moduleResources/es-coreapps/styles/find-patient.css" />

<%@ include file="/WEB-INF/es-template/header.jsp"%>