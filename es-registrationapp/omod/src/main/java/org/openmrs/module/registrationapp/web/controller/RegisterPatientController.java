package org.openmrs.module.registrationapp.web.controller;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.PersonAddress;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.PersonName;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.module.registrationapp.PatientForm;
import org.openmrs.module.registrationapp.RegistrationConstants;
import org.openmrs.module.registrationapp.api.RegistrationService;
import org.openmrs.module.registrationapp.utils.RegistrationUtils;
import org.openmrs.module.registrationapp.validator.RegisterPatientValidator;
import org.openmrs.validator.PatientValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegisterPatientController {

	protected final Log log = LogFactory.getLog(getClass());

	@Autowired
	PatientValidator patientValidator;

	@RequestMapping(value = RegistrationConstants.REGISTER_PATIENT_URL, method = RequestMethod.GET)
	public String inputPatient(Model model) {
		model.addAttribute("patientModel", new PatientForm());
        model.addAttribute("user", Context.getAuthenticatedUser());
		return RegistrationConstants.REGISTER_PATIENT_PAGE_LOCATION;
	}

	@RequestMapping(value = RegistrationConstants.REGISTER_PATIENT_URL, method = RequestMethod.POST)
	public String savePatient(@ModelAttribute("patientModel") PatientForm patientForm, BindingResult result) {

		if (Context.isAuthenticated()) {
			// TODO: Dummy
			if (patientForm != null) {
				patientForm.setBirthDate(new Date());
			}

			// Validate form data
			RegisterPatientValidator validator = new RegisterPatientValidator();
			validator.validate(patientForm, result);
			if (result.hasErrors()) {
				return RegistrationConstants.REGISTER_PATIENT_PAGE_LOCATION;
			}

			// Save avatar
			String avatarId = RegistrationUtils.saveAvatarFile(patientForm.getAvatar());

			// Retrieve patient information from the form data
			Patient patient = getPatientFromFormData(patientForm);
			resolvePersonAttributeFields(patientForm, avatarId, patient);
			
			// Register patient
			try {
				RegistrationService registrationService = Context.getService(RegistrationService.class);
				patient = registrationService.registerPatient(patient);

			} catch (APIException e) {
				log.error("Saving new patient has an error: ", e);
			}

		}

		return RegistrationConstants.REGISTER_PATIENT_PAGE_LOCATION;
	}

	private Patient getPatientFromFormData(PatientForm patientForm) {

		if (patientForm == null) {
			throw new APIException("The Patient form data cannot be null");
		}

		Patient patient = new Patient();

		// Person name
		if (patientForm.getFirstName() != null && patientForm.getLastName() != null) {

			PersonName personName = new PersonName(patientForm.getFirstName(), null, patientForm.getLastName());
			personName.setDateCreated(new Date());
			patient.addName(personName);
		}

		// Address
		if (patientForm.getAddress() != null) {
			PersonAddress personAddress = new PersonAddress();
			personAddress.setAddress1(patientForm.getAddress());
			personAddress.setDateCreated(new Date());
			patient.addAddress(personAddress);
		}

		// Gender
		if (patientForm.getGender() != null) {
			patient.setGender(patientForm.getGender());
		}

		// Birth date
		if (patientForm.getBirthDate() != null) {
			patient.setBirthdate(patientForm.getBirthDate());
		}

		return patient;
	}

	private void resolvePersonAttributeFields(PatientForm patientForm, String avatarId, Patient patient) {

		if (patientForm == null || patient == null) {
			throw new APIException("Input data cannot be null");
		}

		List<PersonAttributeType> attributeTypes = Context.getPersonService().getAllPersonAttributeTypes();

		PersonAttribute attribute = null;
		for (PersonAttributeType personAttributeType : attributeTypes) {
			if (personAttributeType == null) {
				continue;
			}

			String attributeTypeName = personAttributeType.getName();
			if (StringUtils.isEmpty(attributeTypeName)) {
				continue;
			}

			// Identity Card
			if (RegistrationConstants.IDENTITY_CARD_FIELD.equals(attributeTypeName)) {
				if (patientForm.getIdentityCard() != null) {
					attribute = new PersonAttribute(personAttributeType, patientForm.getIdentityCard());
					patient.addAttribute(attribute);
				}
			}
			// Phone Number
			else if (RegistrationConstants.PHONE_NUMBER_FIELD.equals(attributeTypeName)) {
				if (patientForm.getPhoneNumber() != null) {
					attribute = new PersonAttribute(personAttributeType, patientForm.getPhoneNumber());
					patient.addAttribute(attribute);
				}
			}
			// Email
			else if (RegistrationConstants.EMAIL_FIELD.equals(attributeTypeName)) {
				if (patientForm.getEmail() != null) {
					attribute = new PersonAttribute(personAttributeType, patientForm.getEmail());
					patient.addAttribute(attribute);
				}
			}
			// Married Status
			else if (RegistrationConstants.MARITAL_STATUS_FIELD.equals(attributeTypeName)) {
				attribute = new PersonAttribute(personAttributeType, patientForm.getMaritalStatus());
				patient.addAttribute(attribute);
			}
			// Avatar ID
			else if (RegistrationConstants.AVATAR_ID_FIELD.equals(attributeTypeName)) {
				if (avatarId != null) {
					attribute = new PersonAttribute(personAttributeType, avatarId);
					patient.addAttribute(attribute);
				}
			}
		}
	}

}
