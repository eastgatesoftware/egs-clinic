package org.openmrs.module.registrationapp.utils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.util.OpenmrsUtil;
import org.springframework.web.multipart.MultipartFile;

import com.mchange.util.AssertException;

public class RegistrationUtils {

	protected static final Log log = LogFactory.getLog(RegistrationUtils.class);

	private static String PATIENT_IMAGE_DIRECTORY = OpenmrsUtil.getApplicationDataDirectory() + "/patient_images/";

	private RegistrationUtils() {
		throw new AssertException();
	}

	public static String generateImageId() {
		return UUID.randomUUID().toString() + ".jpg";
	}

	public static String saveAvatarFile(MultipartFile multipart) {

		if (multipart == null || multipart.getSize() == 0) {
			return null;
		}

		// Generate AvatarID
		String avatarId = RegistrationUtils.generateImageId();
		
		createPatientImageDirectory();
		
		// Save file
		File avatarFile = new File(PATIENT_IMAGE_DIRECTORY + avatarId);
		try {
			multipart.transferTo(avatarFile);

		} catch (IOException e) {
			log.error("Saving patient image has an error: " + e);
			return null;
		}

		return avatarId;
	}
	
	private static void createPatientImageDirectory() {

		// Check whether patient image directory exists
		// If this directory does not exist, then create a new directory
		File imageDirectory = new File(PATIENT_IMAGE_DIRECTORY);
		if (!imageDirectory.exists()) {
			try {
				FileUtils.forceMkdir(imageDirectory);

			} catch (IOException e) {
				log.error("Error occurs when making patient image directory: " + e);
			}
		}
	}
}
