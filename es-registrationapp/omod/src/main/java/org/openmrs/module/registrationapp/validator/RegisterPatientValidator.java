package org.openmrs.module.registrationapp.validator;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.APIException;
import org.openmrs.module.registrationapp.PatientForm;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class RegisterPatientValidator implements Validator {

	protected final Log log = LogFactory.getLog(getClass());

	@Override
	public boolean supports(Class<?> clazz) {
		return RegisterPatientValidator.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		if (log.isDebugEnabled()) {
			log.debug(this.getClass().getName()
					+ ": Validating patient data from the register patient form!");
		}

		PatientForm patientForm = (PatientForm) obj;
		if (patientForm == null) {
			throw new APIException("The Patient form data cannot be null");
		}

		// Make sure they chose a gender
		if (StringUtils.isBlank(patientForm.getGender())) {
			errors.rejectValue("gender", "Person.gender.required");
		}

		// Check patients birth date
		if (patientForm.getBirthDate() != null) {
			if (patientForm.getBirthDate().after(new Date())) {
				errors.rejectValue("birthDate", "error.date.future");

			} else {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.add(Calendar.YEAR, -120); // Patient cannot be older
													// than 120

				if (patientForm.getBirthDate().before(calendar.getTime())) {
					errors.rejectValue("birthDate", "error.date.nonsensical");
				}
			}
		} else {
			errors.rejectValue("birthDate", "error.required");
		}
	}

}
