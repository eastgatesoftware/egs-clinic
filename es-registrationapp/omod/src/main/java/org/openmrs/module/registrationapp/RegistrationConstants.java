package org.openmrs.module.registrationapp;

import com.mchange.util.AssertException;

public class RegistrationConstants {

	private RegistrationConstants() {
		throw new AssertException();
	}

	// *** Url ***
	public static final String REGISTER_PATIENT_URL = "/module/registrationapp/registerPatient.form";

	// *** Jsp file location ***
	public static final String REGISTER_PATIENT_PAGE_LOCATION = "/module/registrationapp/pages/registerPatient";

	// *** Fields ***
	public static final String IDENTITY_CARD_FIELD = "Identity Card";
	public static final String PHONE_NUMBER_FIELD = "Phone Number";
	public static final String EMAIL_FIELD = "Email";
	public static final String MARITAL_STATUS_FIELD = "Marital Status";
	public static final String AVATAR_ID_FIELD = "Avatar ID";
}
