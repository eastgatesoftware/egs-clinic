<%@ include file="localHeader.jsp"%>
<%@ include file="/WEB-INF/es-template/mainLeftMenu.jsp"%>

<div xmlns="http://www.w3.org/1999/xhtml"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:openmrs="urn:jsptld:/WEB-INF/taglibs/openmrs.tld">
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header register-patient-header">
			<h1 class="text-light-blue">REGISTER PATIENT</h1>
		</section>

		<!-- Main content -->
		<form:form method="post" commandName="patientModel"
			enctype="multipart/form-data">
			<section class="content">
				<section class="content register-patient-content">
					<div class="row">
						<div class="col-xs-9">
							<div class="table-content">
								<div class="register-patient-form">

									<table class="table table-responsive no-border">
										<tr>
											<td class="column-header">Name</td>
											<td>
												<div class="form-group ">
													<label class="control-label " for="firstname">
														First name </label>
													<div class="inner-addon left-addon" id="firstname">
														<i class="fa fa-user"></i>
														<form:input cssClass="form-control" path="firstName" />
													</div>
												</div>
											</td>
											<td>
												<div class="form-group ">
													<label class="control-label" for="lastname"> Last
														name </label>
													<form:input cssClass="form-control" path="lastName" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="column-header">Gender</td>
											<td>
												<div class="radio es-radio es-radio-info radio-inline">
													<form:radiobutton path="gender" value="male" />
													<label for="gender"> Male </label>
												</div>
											</td>
											<td>
												<div class="radio es-radio es-radio-info radio-inline">
													<form:radiobutton path="gender" value="female" />
													<label for="gender"> Female </label>
												</div>
											</td>
										</tr>
										<tr>
											<td class="column-header">Birthday</td>
											<td>
												<div class="form-group">
													<select class="select form-control" id="day" name="day">
														<option value="">Day</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</div>
											</td>
											<td>
												<div class="form-group">
													<select class="select form-control" id="month" name="month">
														<option value="">Month</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</div>
											</td>
											<td>
												<div class="form-group">
													<select class="select form-control" id="year" name="year">
														<option value="">Year</option>
														<option value="2010">2010</option>
														<option value="2011">2011</option>
														<option value="2012">2012</option>
														<option value="2013">2013</option>
														<option value="2014">2014</option>
													</select>
												</div>
											</td>
										</tr>
										<tr>
											<td class="column-header">Số CMND</td>
											<td colspan="2">
												<div class="form-group ">
													<form:input cssClass="form-control" path="identityCard" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="column-header">Address</td>
											<td colspan="2">
												<div class="form-group ">
													<form:input cssClass="form-control" path="address" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="column-header">Phone</td>
											<td colspan="2">
												<div class="form-group ">
													<form:input cssClass="form-control" path="phoneNumber" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="column-header">Email</td>
											<td colspan="2">
												<div class="form-group ">
													<form:input cssClass="form-control" path="email" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="column-header">Married</td>
											<td>
												<div class="radio es-radio es-radio-info radio-inline">
													<form:radiobutton path="maritalStatus" value="yes" />
													<label for="maritalStatus"> Yes </label>
												</div>
											</td>
											<td class="column-header">
												<div class="radio es-radio es-radio-info radio-inline">
													<form:radiobutton path="maritalStatus" value="no" />
													<label for="maritalStatus"> No </label>
												</div>
											</td>
										</tr>
									</table>

								</div>
							</div>
						</div>
						<%-- <div class="col-xs-3">
                            <div class="patient-avatar-upload-wrapper">
                                <img class="img img-responsive"
                                    src="${pageContext.request.contextPath}/moduleResources/registrationapp/theme/dist/img/avatar5.png"
                                /> <input id="avatar" name="avatar" class="input-file" type="file" style="display: none;">
                                <div class="fa-stack fa-2x patient-avatar-upload-button">
                                    <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-camera fa-stack-1x fa-inverse es-icon-1x"></i>
                                </div>
                            </div>
                        </div> --%>
					</div>
				</section>
				<section class="register-patient-action">
					<div class="row">
						<div class="col-xs-6 text-left">
							<button class="btn btn-md btn-default uppercase">Cancel</button>
						</div>
						<div class="col-xs-6 text-right">
							<button type="submit" class="btn btn-md btn-primary uppercase">
								<i class="fa fa-floppy-o" aria-hidden="true"></i> Save
							</button>
						</div>
					</div>
				</section>

			</section>
		</form:form>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
</div>
<%@ include file="localFooter.jsp"%>