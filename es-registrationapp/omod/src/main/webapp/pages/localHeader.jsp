<%@ include file="/WEB-INF/es-template/include.jsp"%>
<%@ include file="/WEB-INF/es-template/head.jsp"%>

<openmrs:message var="pageTitle" code="registrationapp.title" scope="page"/>
<openmrs:htmlInclude file="/moduleResources/registrationapp/styles/register-patient.css" />

<%@ include file="/WEB-INF/es-template/header.jsp"%>