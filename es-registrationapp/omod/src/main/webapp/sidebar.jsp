  <!-- Left side column. contains sidebar -->
  <aside class="main-sidebar">

    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <!-- Sidebar toggle button-->
        <li class="sidebar-toggle-li">
          <div class="sidebar-toggle-text">
              <span><spring:message code="Sidebar.menu" /></span>
          </div>
          <div class="sidebar-toggle-button">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
              <span class="sr-only"><spring:message code="Sidebar.menu" /></span>
            </a>  
          </div>
        </li>
        <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></i><span> <spring:message code="Sidebar.findPatient"/></span></a></li>
        <li><a href="#"><i class="fa fa-check-circle" aria-hidden="true"></i><span> <spring:message code="Sidebar.activeVisit"/></span></a></li>
        <li <c:if test='<%= request.getRequestURI().contains("registrationapp/registerPatient") %>'>class="active"</c:if>>	
	       	<a
			href="${pageContext.request.contextPath}/module/registrationapp/registerPatient.form">
        	<i class="fa fa-plus-circle" aria-hidden="true"></i>
                <span>
                    <spring:message code="Sidebar.registrationPatient"/>
                </span>
        	</a>
       	</li>
        <li><a href="#"><i class="fa fa-calendar-o" aria-hidden="true"></i><span> <spring:message code="Sidebar.calendar"/></span></a></li>
        <li class="sidebar-menu-divider"></li>
        <li class="header"><span><spring:message code="Sidebar.setting" /></span></li>       
        <li class="setting-option"><a href="#"><i class="fa fa-desktop" aria-hidden="true"></i><span> <spring:message code="Sidebar.dataManagement"/></span></a></li>       
        <li class="setting-option"><a href="#"><i class="fa fa-cog" aria-hidden="true"></i><span> <spring:message code="Sidebar.configMetadata"/></span></a></li>       
        <li class="setting-option"><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i><span> <spring:message code="Sidebar.systemAdmin"/></span></a></li>       
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>