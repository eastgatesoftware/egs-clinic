$(document).ready(function() {
    $('#example').DataTable({
        "pageLength": 5,
    });
    
    $('.pagination').on('click', 'li', function() {
        var page = $(this).attr('data-dt-idx');
        page = page || 0;
        var data = {
    		skip : 0,
			take : 5
        }
        
        $.ajax({
        	type : "GET",
			contentType : "application/json",
			url : "${home}search/api/getSearchResult",
			data : JSON.stringify(data),
			dataType : 'json',
			timeout : 100000,
			success: function(data) {
				alert(data);
			}
        });
    });
    
});