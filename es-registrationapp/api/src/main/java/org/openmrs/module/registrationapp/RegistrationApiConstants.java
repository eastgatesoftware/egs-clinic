package org.openmrs.module.registrationapp;

import com.mchange.util.AssertException;

public class RegistrationApiConstants {
	
	private RegistrationApiConstants() {
		throw new AssertException();
	}
	
	public static final String EGS_PATIENT_INDENTIFIER_TYPE_NAME = "EGS Identification Number";

}
