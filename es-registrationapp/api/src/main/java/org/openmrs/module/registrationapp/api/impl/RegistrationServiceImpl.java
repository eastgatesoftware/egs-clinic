/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.registrationapp.api.impl;

import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.PatientIdentifierType;
import org.openmrs.api.APIException;
import org.openmrs.api.LocationService;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.module.idgen.IdentifierSource;
import org.openmrs.module.idgen.api.IdentifierSourceService;
import org.openmrs.module.registrationapp.RegistrationApiConstants;
import org.openmrs.module.registrationapp.api.RegistrationService;

/**
 * It is a default implementation of {@link RegistrationService}.
 */
public class RegistrationServiceImpl extends BaseOpenmrsService implements
		RegistrationService {

	protected final Log log = LogFactory.getLog(this.getClass());

	private static PatientIdentifierType primaryIdentifierType;

	private static IdentifierSource idSource;

	private static Location defaultLocation;

	private PatientService patientService;

	private LocationService locationService;

	@Override
	public Patient registerPatient(Patient patient) {

		if (log.isInfoEnabled()) {
			log.info("Registering new patient...");
		}

		if (patient == null) {
			throw new APIException("Patient cannot be null");
		}

		IdentifierSourceService idSourceService = Context
				.getService(IdentifierSourceService.class);
		if (primaryIdentifierType == null) {
			primaryIdentifierType = Context
					.getPatientService()
					.getPatientIdentifierTypeByName(
							RegistrationApiConstants.EGS_PATIENT_INDENTIFIER_TYPE_NAME);
			if (primaryIdentifierType == null) {
				throw new APIException(
						"Cannot find patient identifier type:"
								+ RegistrationApiConstants.EGS_PATIENT_INDENTIFIER_TYPE_NAME);
			}
		}

		if (idSource == null) {
			idSource = idSourceService.getAutoGenerationOption(
					primaryIdentifierType).getSource();
			if (idSource == null) {
				throw new APIException("Cannot find identifier source with id:"
						+ idSource);
			}
		}

		if (defaultLocation == null) {
			defaultLocation = locationService.getDefaultLocation();
			if (defaultLocation == null) {
				throw new APIException(
						"Failed to resolve location to associate to patient identifiers");
			}
		}

		// Create an new identifier for new patient
		if (patient.getPatientIdentifier() == null) {
			if (patient.getBirthdate() == null) {
				throw new APIException("Patient's birth date cannot be null");
			}

			String identifierString = idSourceService.generateIdentifier(
					idSource, patient.getBirthdate(), null);
			PatientIdentifier patientId = new PatientIdentifier(
					identifierString, idSource.getIdentifierType(),
					defaultLocation);
			patient.addIdentifier(patientId);
			patientId.setPreferred(true);
		}

		patient = patientService.savePatient(patient);

		return patient;
	}

	public LocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}

	public PatientService getPatientService() {
		return patientService;
	}

	public void setPatientService(PatientService patientService) {
		this.patientService = patientService;
	}

}