<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page import="org.openmrs.web.WebConstants"%>
<%
    pageContext.setAttribute("msg",
            session.getAttribute(WebConstants.OPENMRS_MSG_ATTR));
    pageContext.setAttribute("msgArgs",
            session.getAttribute(WebConstants.OPENMRS_MSG_ARGS));
    pageContext.setAttribute("err",
            session.getAttribute(WebConstants.OPENMRS_ERROR_ATTR));
    pageContext.setAttribute("errArgs",
            session.getAttribute(WebConstants.OPENMRS_ERROR_ARGS));
    session.removeAttribute(WebConstants.OPENMRS_MSG_ATTR);
    session.removeAttribute(WebConstants.OPENMRS_MSG_ARGS);
    session.removeAttribute(WebConstants.OPENMRS_ERROR_ATTR);
    session.removeAttribute(WebConstants.OPENMRS_ERROR_ARGS);
%>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:c="http://java.sun.com/jsp/jstl/core" xmlns:jsp="http://java.sun.com/JSP/Page"
    xmlns:spring="http://www.springframework.org/tags" xmlns:openmrs="urn:jsptld:/WEB-INF/taglibs/openmrs.tld"
>

<head>
<c:choose>
    <c:when test="${!empty pageTitle}">
        <title>${pageTitle}</title>
    </c:when>
    <c:otherwise>
        <title><openmrs:message code="openmrs.title" /></title>
    </c:otherwise>
</c:choose>

<openmrs:htmlInclude file="/scripts/es-clinic/bootstrap/css/bootstrap.min.css" />
<openmrs:htmlInclude file="/scripts/es-clinic/fonts/fontawesome/css/font-awesome.min.css" />
<openmrs:htmlInclude file="/scripts/es-clinic/plugins/datatables/dataTables.bootstrap.css" />
<openmrs:htmlInclude file="/scripts/es-clinic/dist/css/es.css" />
<openmrs:htmlInclude file="/scripts/es-clinic/dist/css/skins/es-skin.css" />
<openmrs:htmlInclude file="/scripts/es-clinic/dist/css/skins/es-mixins.css" />