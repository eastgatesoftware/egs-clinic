</head>
<body class="hold-transition es-skin sidebar-mini">
    <div class="wrapper">
        <!-- Main Header --> <header class="main-header"> <!-- Logo -->
            <div class="logo">
                <div class="logo-system"><span><spring:message code="header.egs"></spring:message></span></div>
                <div class="logo-arrow-right"><img src="${pageContext.request.contextPath}/scripts/es-clinic/dist/img/arrow-right.png" /></div>
                <div class="nav-route">
                    <ol class="breadcrumb">
                        <li><a href="#">All Patients</a></li>
                        <li class="active">${pageTitle}</li>
                </ol>
            </div>
        </div> <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown registration-desk"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-calendar-check-o" aria-hidden="true"></i> <span><spring:message code="header.registrationDesk" /></span> <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </a>
                            <ul class="dropdown-menu">
                                <li class="header"><spring:message code="header.registrationDeskDesc" /></li>
                                <li>
                                    <ul class="menu">
                                        <li><a href="#">
                                                <div class="pull-left"><img src="${pageContext.request.contextPath}/scripts/es-clinic/dist/img/admin.png" class="img-circle" alt="User Image"></div>
                                                <h4>Patient XXX registered <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                                <p>Bring him to Hospital?</p>
                                        </a></li>
                                        <li><a href="#">
                                                <div class="pull-left"><img src="${pageContext.request.contextPath}/scripts/es-clinic/dist/img/admin.png" class="img-circle" alt="User Image"></div>
                                                <h4>Patient XXX registered <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                                <p>Bring her to Hospital?</p>
                                        </a></li>

                                </ul>
                            </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                        </ul></li>
                        <li class="dropdown user user-menu"><openmrs:authentication>
                                <c:if test="${authenticatedUser != null}">
                                    <a href='#'> <img src="${pageContext.request.contextPath}/scripts/es-clinic/dist/img/admin.png" class="user-image" alt="User Image"> <span class="hidden-xs"> <c:out value="${authenticatedUser.personName}" />
                                    </span> <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </a>
                                </c:if>
                                <c:if test="${authenticatedUser == null}">
                                    <a href='${pageContext.request.contextPath}/login.htm' class="dropdown-toggle" data-toggle="dropdown"> <img src="${pageContext.request.contextPath}/scripts/es-clinic/dist/img/admin.png" class="user-image" alt="User Image"> <span class="hidden-xs"> <openmrs:message code="header.login" />
                                    </span> <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </a>
                                </c:if>
                            </openmrs:authentication>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header"><img src="${pageContext.request.contextPath}/scripts/es-clinic/dist/img/avatar5.png" class="img-circle" alt="User Image">

                                    <p><openmrs:authentication>
                                            <c:if test="${authenticatedUser != null}">
                                                <c:out value="${authenticatedUser.personName}" /> - <small>Member since Nov. 2016</small>
                                            </c:if>
                                        </openmrs:authentication></p></li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-4 text-center"><a href="#">Options 1</a></div>
                                        <div class="col-xs-4 text-center"><a href="#">Options 2</a></div>
                                        <div class="col-xs-4 text-center"><a href="#">Options 3</a></div>
                                </div> <!-- /.row -->
                            </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left"><a href="#" class="btn btn-default btn-flat"> <spring:message code="Navigation.options"></spring:message>
                                    </a></div>
                                    <div class="pull-right"><a href="#" class="btn btn-default btn-flat"><spring:message code="header.logout"></spring:message></a></div>
                            </li>
                        </ul></li>
                        <!-- Control Sidebar Toggle Button -->
                        <li class="logout-control"><openmrs:authentication>
                                <c:if test="${authenticatedUser != null}">
                                    <a href="${pageContext.request.contextPath}/logout"> <span><openmrs:message code="header.logout" /></span> <img src="${pageContext.request.contextPath}/scripts/es-clinic/dist/img/exit.png" class="logout-image" alt="Logout">
                                    </a>
                                </c:if>
                                <c:if test="${authenticatedUser == null}">
                                    <a href="${pageContext.request.contextPath}/index.htm"> <span><openmrs:message code="header.login" /></span> <img src="${pageContext.request.contextPath}/scripts/es-clinic/dist/img/exit.png" class="logout-image" alt="Logout">
                                    </a>
                                </c:if>
                            </openmrs:authentication></li>
                </ul>
            </div>
        </nav>
    </header>